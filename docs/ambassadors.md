---
layout: default
title: Open WIN Ambassadors
has_children: true
nav_order: 5
---


# Open WIN Ambassadors
{: .fs-9 }

Find out about our plans for the Open WIN Ambassadors scheme
{: .fs-6 .fw-300 }

---

The Open WIN Ambassadors will be a team of individuals who are significant contributors to the Open WIN Community. The Ambassador programme will be launched in September 2021.

![Ambassadors as superheros](../../img/undraw_Powerful_re_frhr.svg)

**See the sections below or navigate on the sidebar to find out more about the Open WIN Ambassadors.**
