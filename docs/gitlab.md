---
layout: default
title: Git and GitLab
has_children: true
nav_order: 7
---


# GitLab
{: .fs-9 }

How to use the WIN GitLab instance for your code and documentation
{: .fs-6 .fw-300 }

---

![gitlab-logo](../../img/logo-gitlab.png)

## Overview
GIT is a service which allows version control of material and syncing between local (on your computer) and remote (on a server, accessible through a web browser) versions of material. These remote and local versions are useful for backup and collaboration, where different users can access the same remote content.

WIN provide a git service using the open source [GitLab software](https://about.gitlab.com). We have installed GitLab on our internal servers, so you can use it to maintain your material in a space where we have complete control over who can access it.

GitLab is best used for the management of text-based documents with some pictures (not nifti brain images!). This mainly covers code and documentation. It is also useful for project management by logging issues and milestones, and tracking collaboration.

### Benefits
#### Version control ![version-control](../../img/icon-version-control.png)
GitLab manages your version control. This means that each change you make to the remote repository is tracked and can be reversed. This is helpful for undoing changes (you can revert to earlier points in time) and tracking who has made changes on a collaborative project.

#### Citable research output ![doi](../../img/icon-doi.png)
Your gitlab repository can be assigned a digital object identifier (doi). This means everything containing within your repository can be cited as a research output, be it code or documentation. Find out how to [create a doi for your repository](gitlab/4-2-you-doi.md).

test link: [create a doi for your repository](gitlab/4-2-you-doi)

#### Managed and attributable collaboration
Git and GitLab are an excellent resource for inviting others to contribute to your material. Potential collaborators can be internal to WIN or external. Changes can be incorporated automatically if the collaborator is trusted and known to you (invited directly), or suggested via a "merge request" if the collaborator is not part of your invited group. This process also makes it very easy to follow who has made what contribution, so they can be appropriately attributed for their efforts.

#### GitLab pages
GitLab pages allows you to build professional looking websites, like this one! It is relatively straightforward to build a simple website to share your work with the world, and the options for building in additional complexity are endless.
