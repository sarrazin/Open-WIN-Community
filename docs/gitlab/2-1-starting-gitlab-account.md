---
layout: default
title: 2.1 Your GitLab account
parent: Tutorials
grand_parent: Git and GitLab
has_children: false
nav_order: 4
---


# Access and connect to your GitLab account
{: .fs-8 }

Access your GitLab account
{: .fs-6 .fw-300 }

---

To use the WIN GitLab you will need to register for a WIN IT computing account. Please register for a WIN computing account following this [guide on the WIN intranet](https://sharepoint.nexus.ox.ac.uk/sites/NDCN/FMRIB/IT/User%20Guides/Computing%20Accounts%20-%20requesting%2c%20using%20and%20closing.aspx).

Once you have a computing account, you can access GitLab by visiting [https://git.fmrib.ox.ac.uk](https://git.fmrib.ox.ac.uk) in a web browser and log in (using the LDAP tab) with your WIN IT username and password.

The git server identifies and authenticates you (or rather your computer) using an [SSH key pair](https://www.ssh.com/academy/ssh/public-key-authentication). This means we need to create a key pair to authenticate the connection from your local computer to the remote repository.

From your terminal, follow the instructions here [to create an SSH key pair for your local computer](https://sharepoint.nexus.ox.ac.uk/sites/NDCN/FMRIB/IT/User%20Guides/SSH.aspx#title11)

You will be asked to provide a file name for the key. Leave this blank to accept the default location of ~.ssh/id_rsa.

You will be asked to create a password. This password will be used by the git system to read the contents of your ssh key file (i.e. confirm your identity) when you send information to your repository. Do not use the same password as your WIN IT account password.

Once you have created the key, enter `cd ~` in your terminal, then `cat .ssh/id_rsa.pub` to read the contents of the SSH key file into the terminal. Copy and paste the contents into the SSH keys section of your profile settings on git.fmrib.ox.ac.uk (accessed via User Settings > SSH Keys - the "key" icon on left menu bar.
