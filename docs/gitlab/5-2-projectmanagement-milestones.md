---
layout: default
title: 5.2 GitLab milestones
parent: Tutorials
grand_parent: Git and GitLab
has_children: false
nav_order: 15
---


# GitLab milestones
{: .fs-8 }

Use GitLab milestones to plan and track your project activity
{: .fs-6 .fw-300 }

---

Coming soon
{: .label .label-yellow }
