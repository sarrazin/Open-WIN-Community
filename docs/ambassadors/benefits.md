---
layout: default
title: Benefits for Ambassadors
parent: Open WIN Ambassadors
has_children: false
nav_order: 3
---
# Benefits for Ambassadors
{: .fs-9 }

Find out about how the benefits of becoming an Open WIN Ambassador

{: .fs-6 .fw-300 }

---
In recognition of the time, skill and expertise it takes to be an Open Ambassador, you will receive a number of benefits for taking part. These may include:

- Ambassador profiles on this repository
- Named authorship on tools documentation and the Open WIN repository citation
- Recognition of your contribution on your annual personal development review
- Opportunities to network with likeminded colleagues internally and externally
- Opportunity to shape the policy, structure and governance of the Open WIN Community
- Career advancement and exposure through skills development and project management experience
- Approved time with your line manager to contribute to these activities
- Swag! You want a mug?! We could get mugs! ☕️ 😅

What else would you like? We're aiming to solve the problems which you face, or your barriers to working open. Why do you want to participate? What would motivate others like you?


## Career advancement and exposure
Engaging with open research is good for your CV as many funding and fellowship applications are now requesting and recognising research outputs other than solely papers. For example, preprints, code, datasets and documentation can all be referenced with a digital object identifier (doi) and cited as research outputs. Our hope is that any user of the Open WIN Tools will be able to produce some non-traditional research outputs, however we expect that Ambassadors will stand out as deeply committed to this work.

Ambassadors will have the opportunity to devise and deliver training on these tools, and potentially co-lead a small team working on a specific output. You will also be supported in collaborating with other researchers on open and community projects in neuroimaging ([internal to WIN](https://www.win.ox.ac.uk/open-win/open-win-resources) or an external project for example via [BrainWeb](https://brain-web.github.io/projects/)), or starting your own project or community activity as part of Open WIN. These are both excellent experience and opportunities to show leadership on your CV and community building.

Active participation in this community is also evidence of "Good Citizenship" which can be discussed in new employment and promotion applications. It may involve engaging with Senior Management on committees, and you will have the opportunity to learn and practice project management and committee work outside of your immediate research environment.
