---
layout: default
title: Programme schedule
parent: Open WIN Ambassadors
has_children: false
nav_order: 5
---
# Programme schedule
{: .fs-9 }

Find out about how the Ambassadors programme will be structured and scheduled

{: .fs-6 .fw-300 }

---

**Contents**
- [Technology](#technology)
- [Call structure](#call-structure)
- [Schedule](#schedule)


All activities of the Ambassadors programme will take place online. Any in-person celebratory or team building meetings will be designed to be as accessible as possible and accommodating to the responsibilities of our Ambassadors.

## Technology

We will use a number of online technologies to work efficiently, transparently and reproducibly online. Many of these solutions will provide you with transferable skills which you will be able to incorporate into your own research workflow. Training will be provided to ensure all Ambassadors are comfortable working in this way.

Calls will take place on [Wonder](https://www.wonder.me/) and we will use [hackmd.io](http://hackmd.io/) to write collaborative notes during the call, using markdown so they can be saved directly to a git repository. We may use [padlet](https://padlet.com) for creative exercises.

We will use the [WIN GitLab instance](http://git.fmrib.ox.ac.uk/) to write documentation and manage project work. You will require a WIN IT account to access GitLab.

You will need a [GitHub](http://github.com/) account to use hackmd for call notes (this will be reviewed if we can create a similar in-house gitLab solution).

## Call structure

#### Calls 1:4

During the first month of the programme you will participate in workshops to help you work with git and GitLab, understand more about the value and practices of open science, learn how to use the Open WIN tools, and have and open discussion about policy, incentives, training and monitoring of open science activities in WIN.

#### Calls 5:6

Once you are familiar with the Open WIN infrastructure and working on GitLab, you will be encouraged to decide which tool(s) you would like to develop documentation for. You may also have ideas about which other projects you would like to take on or contribute to. In weeks 5 we will allocate the tools and discuss [what materials we need to create to support uptake](https://git.fmrib.ox.ac.uk/open-science/community/Open-WIN-Community/-/issues/25). In week 6 we will set up your GitLab space to develop documentation and your own projects.  

#### Calls 8:15

Our regular calls will be 90 minutes long; 30 minutes for updates and problem solving, then an optional 60 minutes for co-working. These calls will be held on the first Wednesday of the month.

During the update time, we will first silently document updates together, then read each others updates and pose questions. This process is intended to make efficient use of our time as you will not have to prepare updates in advance or sit and wait for your turn to report back. If anyone would like support, guidance, to address questions from others or seek opinions on any part of their work, they will be invited to open the floor for feedback.

During the 60 minute co-working time we will break out into small flexible groups and work on documentation and/or our open and community projects. This explicit co-working time is scheduled so you can have a dedicated time and space to contribute. You may choose to contribute outside of this time, or arrange co-working times which better suite your needs.

## Schedule

Calendar invitations will be sent to all Ambassadors containing details of the below.

| Call | Date | Time | Topic  |
|---|---|---|---|
| 1 | Wednesday 3 November 2021 | 09:30 - 11:00 | intro and why open science |
| 2 | Wednesday 10 November 2021 | 09:30 - 11:00 | git and gitlab |
| 3 | Wednesday 17 November 2021 | 09:30 - 11:00 | tools demos and questions (meet the developers) |
| 4 | Wednesday 24 November 2021 | 09:30 - 11:00 | policy, incentives, training, monitoring |
| 5 | Wednesday 1 December 2021 | 09:30 - 11:00 | getting into teams, setting up project space |
| 6 | Wednesday 8 December 2021 | 09:30 - 11:00 | getting into teams, setting up project space |
| 7 | Wednesday 5 January 2022 | 09:30 - 11:00 | update and co-working |
| 8 | Wednesday 2 February 2022 | 09:30 - 11:00 | update and co-working |
| 9 | Wednesday 2 March 2022 | 09:30 - 11:00 | Mid point celebration and review |
| 10 | Wednesday 6 April 2022 | 09:30 - 11:00 | update and co-working |
| 11 | Wednesday 4 May 2022 | 09:30 - 11:00 | update and co-working |
| 12 | Wednesday 1 June 2022 | 09:30 - 11:00 | update and co-working |
| 13 | Wednesday 6 July 2022 | 09:30 - 11:00 | update and co-working |
| 14 | Wednesday 3 August 2022 | 09:30 - 11:00 | update and co-working |
| 15 | Wednesday 7 September 2022 | 09:30 - 11:00 | close out and celebration |
