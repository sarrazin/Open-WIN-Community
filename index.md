---
layout: default
title: Home
nav_order: 1
description: "Summary: Find out who we are, our aims for the community and how to get involved!"
has_children: true
---

<!-- ![WIN-logo](docs/img/WIN-h100.png) -->



# Open Research at the Wellcome Centre for Integrative Neuroimaging (WIN)
{: .fs-8 }

Find out how to share your research outputs effectively and responsibly, for improved impact, access and collaboration
{: .fs-6 .fw-300 }

---

This repository is the hub for information on how to employ open research practices at the [Wellcome Centre for Integrative Neuroimaging (WIN)](https://www.win.ox.ac.uk).

The material shared here is written and maintained by an active community of WIN researchers, and is intended to support all WIN members in using the sharing infrastructure created by the [WIN Open Neuroimaging Project](https://www.win.ox.ac.uk/open-neuroimaging).

**Jump straight into one of the sections below, or use the sidebar to find out more about this repository.**

[![tools](img/btn-tools.png)](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/tools/)  [![community](img/btn-community.png)](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/community/)  [![ambassadors](img/btn-ambass.png)](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/ambassadors/)  [![events](img/btn-events.png)](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/events/)  [![gitlab](img/btn-git.png)](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/)




<!-- [Open WIN Community](docs/community.md){: .btn .btn-primary .fs-6 .mb-4 .mb-md-0 .mr-2 }  
Find out about the Open WIN Community and how you can contribute

[Open WIN Ambassadors](docs/abmassadors.md){: .btn .btn-primary .fs-6 .mb-4 .mb-md-0 .mr-2 }  
Find out about our plans for the Open WIN Ambassadors scheme

[Open WIN Events](docs/events.md){: .btn .btn-primary .fs-6 .mb-4 .mb-md-0 .mr-2 }  
Look about for events relevant to our community! -->
